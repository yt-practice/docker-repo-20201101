# ------------------------------------------------------------------------------
# Cargo Build Stage
# ------------------------------------------------------------------------------
# FROM rust:latest as cargo-build
# RUN apt-get update
# RUN apt-get install musl-tools -y
# RUN rustup target add x86_64-unknown-linux-musl
# WORKDIR /usr/src/app
# COPY Cargo.toml Cargo.toml
# RUN mkdir src/
# RUN echo "fn main() {println!(\"if you see this, the build broke\")}" > src/main.rs
# RUN RUSTFLAGS=-Clinker=musl-gcc cargo build --release --target=x86_64-unknown-linux-musl
# RUN rm -f target/x86_64-unknown-linux-musl/release/deps/main*
# COPY . .
# RUN RUSTFLAGS=-Clinker=musl-gcc cargo build --release --target=x86_64-unknown-linux-musl
FROM ekidd/rust-musl-builder:latest as cargo-build
# WORKDIR /home/rust
USER rust
# RUN chown -R rust:rust /home/rust
COPY Cargo.toml Cargo.toml
RUN mkdir src
RUN echo "fn main() { println!(\"if you see this, the build broke\"); std::process::exit(1); }" > src/main.rs
RUN cargo build --release
RUN rm -f target/x86_64-unknown-linux-musl/release/deps/main*
COPY . .
RUN cargo build --release

# ------------------------------------------------------------------------------
# Final Stage
# ------------------------------------------------------------------------------
# FROM alpine:latest
# RUN addgroup -g 1000 app
# RUN adduser -D -s /bin/sh -u 1000 -G app app
# WORKDIR /home/app/bin/
# COPY --from=cargo-build /home/rust/src/target/x86_64-unknown-linux-musl/release/main .
# RUN chown app:app main
# USER app
# ENTRYPOINT ["./main"]
# CMD ["./main"]
FROM scratch
WORKDIR /home/app/bin/
COPY --from=cargo-build /home/rust/src/target/x86_64-unknown-linux-musl/release/main .
ENTRYPOINT ["./main"]
