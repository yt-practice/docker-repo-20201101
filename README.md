# run

```sh
docker run --rm registry.gitlab.com/yt-practice/docker-repo-20201101/image:latest
```

# version

```sh
docker run --rm registry.gitlab.com/yt-practice/docker-repo-20201101/image:latest --version
```

# help

```sh
docker run --rm registry.gitlab.com/yt-practice/docker-repo-20201101/image:latest --help
```

# clone

```sh
git clone git@gitlab.com:yt-practice/docker-repo-20201101.git
cd docker-repo-20201101
docker build -t docker-repo-20201101 .
docker run --rm -it docker-repo-20201101
```
