use clap::{App, Arg, SubCommand};

fn main() {
  let matches = App::new("docker-repo-20201101")
    .version("0.1.1")
    .author("ytoune <tone@re-knock.io>")
    .about("gitlab container registry の練習用に作ったやつ")
    .arg(
      Arg::with_name("user")
        .short("u")
        .long("user")
        .help("あなたの名前")
        .takes_value(true),
    )
    .subcommand(
      SubCommand::with_name("test")
        .about("テスト")
        .version("0.0.0")
        .arg(
          Arg::with_name("debug")
            .short("d")
            .help("print debug information verbosely"),
        ),
    )
    .get_matches();

  if let Some(matches) = matches.subcommand_matches("test") {
    if matches.is_present("debug") {
      println!("Printing debug info...");
    }
    println!("テストないです");
    return;
  }

  if let Some(name) = matches.value_of("user") {
    println!("こんにちは {}", name);
  } else {
    println!("Hello, world!");
  }
}
